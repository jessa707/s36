const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/', (request, response) => {
	// the data comes from result is from TaskController.js
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTask().then((result) => {
		response.send(result)
	})
})

// Update a task
// /:id/update - urlparameter
router.patch('/:id', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Delete Task

router.delete('/:id', (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// =================  ACTIVITY SESSION ===============================

// get Specific Task
router.get('/:id', (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// update Specific Task
router.patch('/:id/complete', (request, response) => {
	TaskController.updateSpecificTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})



module.exports = router


	
